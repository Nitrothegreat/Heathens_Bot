const request = require("request-promise-native");
const inspirobotAPIURL = "https://inspirobot.me/api?generate=true";

exports.run = (client, message, args) => {
    let reqOptions = {
        method: "GET",
        uri: inspirobotAPIURL
    };

    request(reqOptions)
        .then(function(res){
            message.channel.send(res);
        })
}