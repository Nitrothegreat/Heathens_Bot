/*
 * How to display:
 * Bold - Right letter, right spot
 * Italic - Right letter, wrong spot
 * Strikethrough - Bad letter
 * 
 * Take in 5 letter word as input, throw error if not a word in dictionary or length is wrong
 * Need a dictionary
 * Need storage of overtime scores:
 *  {   
 *      [
 *          {
 *              "playerId": "123", //discord id
 *              "scoreCounts": [ 
 *                  "1": "0", //"score": "count"
 *                  "2": "1", //"score": "count"
 *                  "3": "3", //"score": "count"
 *                  "4": "6", //"score": "count"
 *                  "5": "4", //"score": "count"
 *                  "6": "3", //"score": "count"
 *              ]
 *          },
 *          etc....
 *      ]
 *  }
 * 
 * Need storage of current game (changes at midnight CST):
 * {
 *     "winWord": "IRATE",
 *     "date": "2021-02-01",
 *     "games": [
 *          {
 *              "playerId": "123",
 *              "guesses": [
 *                  "1": "PENIS",
 *                  "2": "WENIS"
 *              ]
 *          }
 *     ]
 * }
 * 
 * Need method to format text depending on win word
 * Need method to display on wordle channel. Use the green, yellow, black box formatting. 
 * *word* = italics
 * **word** = bold
 * ~~word~~ = strikethrough
 */