exports.run = (client, message, args) => {
    let helpMsgArr = [
        "!ping: Will message the channel with your delay to the discord server in ms.",
        "!pricecheck [search term]: Will lookup the given argument on cheapshark.com and list deals on games with given search term.",
        "!inspirobot: Will display random \"inspirational\" message from inspirobot.me",
        "!help: You're looking at it.",
        "!hal: Will message channel with random HAL 9000 quote.",
        "!source: Posts gitlab page where my code is stored."
    ];
    let helpMsg = "";

    for(let msg in helpMsgArr){
        helpMsg += helpMsgArr[msg] + "\n";
    }

    message.channel.send(helpMsg);
}