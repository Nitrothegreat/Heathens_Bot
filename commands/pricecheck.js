const request = require("request-promise-native");
const cheapSharkDealURL = "http://www.cheapshark.com/redirect?dealID=";
const tooManyResultsErrorMsg = "Too many results found for search term. Please refine search to return less results.";

exports.run = (client, message, args) => {
    let argsJoined = args.join(" ");
    let cheapSharkURLWithTitle = `http://www.cheapshark.com/api/1.0/games?title=${argsJoined}&limit=10`;
    let reqOptions = {
        method: "GET",
        uri: cheapSharkURLWithTitle
    };

    request(reqOptions)
        .then(function(res){
            let result = JSON.parse(res);
            let msgRes = {
                embed: {
                    title: `Cheap Shark Deal(s) for ${argsJoined}`,
                    timestamp: new Date(),
                    fields : []
                }
            };

            if(result && result.length > 0){
                for(let game of result) {
                    if(msgRes.embed.fields.length <= 23){
                        msgRes.embed.fields.push({
                            name: "Title",
                            value: game["external"]
                        });
                        msgRes.embed.fields.push({
                            name: "Current Deal Price",
                            value: "$" + game["cheapest"]
                        });
                        msgRes.embed.fields.push({
                            name: "Link to Deal",
                            value: cheapSharkDealURL + game["cheapestDealID"]
                        });
                    }
                }
            }
            else{
                msgRes.embed.description = "No results found";
            }

            console.log(JSON.stringify(msgRes, null, 4));

            //TODO: Research what to check for message too long error
            message.channel.send(msgRes).catch(console.error);
        })
        .catch(function(err){
            console.log(err);
        })
};