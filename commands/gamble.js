const fs = require("fs");
//dataPath Format: [{"userId" : "123", "currAmt" : "999"}]
let dataPath = './data/Gamble_Player_Storage.json';

exports.run = (client, message, args) => {
    let rawData = fs.readFileSync(dataPath);
    let scoreData = JSON.parse(rawData);
    let clientUserId = client.user.id;
    let currentUser;

    if(scoreData.length === 0){
        currentUser = makeNewPlayerEntry(scoreData, client);
    }
    else{
        currentUser = scoreData.filter(userObj => userObj.userId === clientUserId);
        if(!currentUser){
            currentUser = makeNewPlayerEntry(scoreData, client);
        }
    }

    makeBet(currentUser);
};

//TODO: Should accept % or specific amount (e.g. 42% or 125)
function makeBet(){

}

function makeNewPlayerEntry(scoreData, client){
    let newPlayer = {userId: client.user.id, currAmt: 100};
    scoreData.push(JSON.stringify(newPlayer));

    fs.writeFileSync(dataPath, JSON.stringify(scoreData), (err) => {
        if (err) throw err;
    });

    return newPlayer;
}